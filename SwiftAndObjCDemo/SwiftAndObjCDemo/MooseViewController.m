//
//  MooseViewController.m
//  SwiftAndObjCDemo
//
//  Created by Jeffrey Ondich on 5/5/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

#import "MooseViewController.h"

@implementation MooseViewController

- (void)viewDidLoad {
    // Prepare the UIImageView
    NSString *path = [[NSBundle mainBundle] pathForResource:@"moose" ofType:@"jpg"];
    UIImage *mooseImage = [UIImage imageWithContentsOfFile:path];
    UIImageView *mooseImageView = [[UIImageView alloc] initWithImage:mooseImage];
    self.view.backgroundColor = [UIColor greenColor];
    [self.view addSubview:mooseImageView];

    // Add constraints for Auto Layout
    
    // It is so easy to forget to do this. Without it, you end up too many constraints
    // and confusing layout behavior as the system tries to resolve them all.
    mooseImageView.translatesAutoresizingMaskIntoConstraints = NO;

    NSDictionary *viewsDictionary = @{@"mooseImageView":mooseImageView, @"topGuide":self.topLayoutGuide, @"bottomGuide":self.bottomLayoutGuide};

    NSArray *horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-16-[mooseImageView]-16-|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:viewsDictionary];
    [self.view addConstraints:horizontalConstraints];
    
    NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topGuide]-16-[mooseImageView]-16-[bottomGuide]"
                                                                           options:0
                                                                           metrics:nil
                                                                             views:viewsDictionary];
    [self.view addConstraints:verticalConstraints];
}

@end
