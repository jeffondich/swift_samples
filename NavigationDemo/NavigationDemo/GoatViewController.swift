//
//  CowViewController.swift
//  NavigationDemo
//
//  Created by Jeffrey Ondich on 4/15/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

protocol GoatDelegate {
    func goatNameDidChange(name: String?)
}

class GoatViewController: UIViewController {
    var goatName: String?
    var delegate: GoatDelegate?
    @IBOutlet weak var goatNameField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.goatName != nil {
            self.goatNameField?.text = self.goatName!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if parent == nil {
            // parent is nil when the GoatViewController is removed from the UINavigationController
            // For this program, that's exactly when we want to alert the main ViewController
            // of the goat's name change.
            if self.goatNameField != nil {
                self.goatName = self.goatNameField!.text
            }
            self.delegate?.goatNameDidChange(self.goatName)
        }
    }
}
