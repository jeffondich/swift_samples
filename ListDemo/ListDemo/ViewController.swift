//
//  ViewController.swift
//  ListDemo
//
//  Created by Jeffrey Ondich on 4/9/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var animalsTableView: UITableView?
    @IBOutlet weak var animalSoundLabel: UILabel?
    
    let cellIdentifier = "AnimalsCell"
    let animals = ["cat", "dog", "cow", "sheep", "crow", "chicken", "pig", "snake", "pikachu"]
    let sounds = ["meow", "woof", "moo", "baa", "caw", "buck-buck", "oink", "ssss", "pikachu"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animalsTableView?.delegate = self
        self.animalsTableView?.dataSource = self
        self.animalSoundLabel?.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.animals.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: self.cellIdentifier)
            cell!.selectionStyle = .None
            cell!.accessoryType = UITableViewCellAccessoryType.None
        }
        
        cell!.textLabel?.text = self.animals[indexPath.row]

        return cell!
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 40
    }

    func tableView(tableView: UITableView, willDeselectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.None
        return indexPath
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let sound = self.sounds[indexPath.row]
        self.animalSoundLabel?.text = "\"\(sound)\""

        
        var cell: UITableViewCell? = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
    }
}

