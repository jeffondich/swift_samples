//
//  DynamicLayoutViewController.swift
//  AutoLayoutDemo
//
//  Created by Jeffrey Ondich on 4/12/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

class DynamicLayoutViewController: UIViewController {
    var viewsDictionary: Dictionary<String, AnyObject>?

    override func viewDidLoad() {
        super.viewDidLoad()

        // The goal here is to set up views like in the Two Rects view controller in
        // Main.storyboard. We've changed green to blue just to make it easy to tell which
        // is which.

        // First, instantiate the two views, set their colors, and add them to the
        // view controller's view.
        var topView: UIView = UIView()
        topView.setTranslatesAutoresizingMaskIntoConstraints(false)
        topView.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.5, alpha: 1.0)
        self.view.addSubview(topView)
        
        var bottomView: UIView = UIView()
        bottomView.setTranslatesAutoresizingMaskIntoConstraints(false)
        bottomView.backgroundColor = UIColor(red: 0.5, green: 0.1, blue: 0.1, alpha: 1.0)
        self.view.addSubview(bottomView)

        // Prepare the views dictionary we'll use to supply views to the visual format strings
        self.viewsDictionary = ["topView":topView, "bottomView":bottomView, "topGuide":self.topLayoutGuide, "bottomGuide":self.bottomLayoutGuide]

        // Constraints to keep the two rectangles equal in size
        self.addConstraints(self.view, formatString: "H:[topView(==bottomView)]")
        self.addConstraints(self.view, formatString: "V:[topView(==bottomView)]")

        // Constraints to position the rectangles in relation to the parent view and each other
        self.addConstraints(self.view, formatString: "H:|-16-[topView]-16-|")
        self.addConstraints(self.view, formatString: "H:|-16-[bottomView]-16-|")
        self.addConstraints(self.view, formatString: "V:[topGuide]-16-[topView]-16-[bottomView]-16-[bottomGuide]")
    }
    
    func addConstraints(v: UIView, formatString: String) {
        var constraints: Array = NSLayoutConstraint.constraintsWithVisualFormat(formatString, options: NSLayoutFormatOptions(0), metrics: nil, views: self.viewsDictionary!)
        v.addConstraints(constraints)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
