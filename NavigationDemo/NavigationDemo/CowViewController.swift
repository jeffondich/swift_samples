//
//  CowViewController.swift
//  NavigationDemo
//
//  Created by Jeffrey Ondich on 4/15/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

protocol CowDelegate {
    func cowNameDidChange(name: String?)
}

class CowViewController: UIViewController {
    var cowName: String?
    var delegate: CowDelegate?
    @IBOutlet weak var cowNameField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.cowName != nil {
            self.cowNameField?.text = self.cowName!
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func didMoveToParentViewController(parent: UIViewController?) {
        if parent == nil {
            // parent is nil when the CowViewController is removed from the UINavigationController
            // For this program, that's exactly when we want to alert the main ViewController
            // of the cow's name change.
            if self.cowNameField != nil {
                self.cowName = self.cowNameField!.text
            }
            self.delegate?.cowNameDidChange(self.cowName)
        }
    }
}
