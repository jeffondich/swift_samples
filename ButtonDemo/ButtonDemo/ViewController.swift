//
//  ViewController.swift
//  ButtonDemo
//
//  Created by Jeffrey Ondich on 4/7/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func handleCowButton() {
        println("Moo")
        var alert = UIAlertController(title: "The Cow Says...", message: "Moo", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func handlePigButton() {
        println("Oink")
        var alert = UIAlertController(title: "The Pig Says...", message: "Oink", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

