//
//  MainViewController.swift
//  SwiftAndObjCDemo
//
//  Created by Jeffrey Ondich on 4/15/15.
//  Copyright (c) 2015 Jeff Ondich. All rights reserved.
//
//  This sample application illustrates navigation using UINavigationController.
//  We have four view controllers in the app:
//
//  1. MainViewController, the root view controller, shows three buttons that will
//     trigger navigation to three other VCs.
//
//  2. MooseViewController is handled entirely within the Main.storyboard.
//     Tap the Moose button in the root VC, and you go to the Moose VC.
//     No information beyond the navigation itself passes between the two VCs.
//
//  3. CowViewController is handled in Main.storyboard, too, but we use a segue
//     to capture the navigation from the root VC to the Cow VC, so we can send
//     data from MainViewController to CowViewController. In addition, CowViewController
//     defines a delegate protocol, via which the Cow VC can send information
//     back to the root VC.
//
//  4. GoatViewController is like CowViewController, but instead of handling all
//     the navigation in Main.storyboard, we do it manually in handleGoatButton.
//     There, we instantiate and initialize the Goat VC via Goat.storyboard, and
//     then we call the UINavigationController's pushViewController method to
//     navigate to the Goat VC. Information passes between MainViewController
//     and GoatViewController as in the Cow example--via instance variables
//     in GoatViewController to pass data forward, and via the GoatDelegate protocol
//     to pass data back.
//

import UIKit

class MainViewController: UIViewController, CowDelegate, GoatDelegate {
    @IBOutlet weak var mooseButton: UIButton?
    @IBOutlet weak var cowButton: UIButton?
    @IBOutlet weak var goatButton: UIButton?

    var cowName: String?
    var goatName: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the title on the Cow and Goat buttons. Since that's exactly what the
        // CowDelegate.cowNameDidChange and GoatDelegate.goatNameDidChange methods
        // do anyway, we use those delegate methods as convenience methods here. If
        // we later needed the delegate methods to do other work, we would need to
        // do the button titles separately.
        self.cowNameDidChange("Bessie")
        self.goatNameDidChange("Billy")
        
        // Since our GoatViewController is intended to be an all-in-code example,
        // we need to set up the
    }

    // We handle the Cow button, which is hooked to the CowViewController in
    // Main.storyboard, via the segue that we set up in the storyboard. We're sending
    // information to the CowViewController (i.e. the current cow name) via the
    // CowViewController's own cowName instance variable. We also
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier != nil && segue.identifier! == "cowSegue" {
            var cowVC = segue.destinationViewController as! CowViewController

            // Send information (i.e. the current value of the cow name) to the
            // CowViewController via its own cowName instance variable.
            cowVC.cowName = self.cowName

            // We're going to receive information back from the CowViewController
            // via the CowDelegate protocol method. So we assign this ViewController
            // to be the CowViewController's delegate. That way, when the time comes,
            // our own cowNameDidChange method will get called by CowViewController.
            cowVC.delegate = self
        }
    }

    @IBAction func handleMooseButton() {
        let mooseVC = MooseViewController()
        self.navigationController?.pushViewController(mooseVC, animated: true)        
    }
    
    @IBAction func handleGoatButton() {
        // Here, we instantiate a GoatViewController and navigate to it directly in
        // code. We could do all of the initialization here in this method, manually.
        // Alternatively, we can use a standalone storyboard to do most of the initialization
        // (e.g. the Auto Layout, the assignment of the goat picture to the UIImageView,
        // etc.). Then, you can load the GoatViewController directly from its storyboard.
        // Here, we have chosen the latter approach.
        
        // Look at Goat.storyboard, select the view controller, and look at the Identity
        // Inspector tab, you'll see that we have set the Storyboard ID to "goatViewController".
        // That allows us to refer to that particular view controller template here in our code,
        // which lets us load a fully initialized GoatViewController from Goat.storyboard.
        let storyboard = UIStoryboard(name: "Goat", bundle: nil)
        let goatVC = storyboard.instantiateViewControllerWithIdentifier("goatViewController") as! GoatViewController

        // Now we do just as we did with the CowViewController in prepareForSegue above.
        goatVC.goatName = self.goatName
        goatVC.delegate = self

        self.navigationController?.pushViewController(goatVC, animated: true)
    }
    
    // MARK: - CowDelegate
    func cowNameDidChange(name: String?) {
        if name != nil {
            self.cowName = name
        }
        self.cowButton?.setTitle("Cow: \(name!)", forState: UIControlState.Normal)
    }
    
    // MARK: - GoatDelegate
    func goatNameDidChange(name: String?) {
        if name != nil {
            self.goatName = name
        }
        self.goatButton?.setTitle("Goat: \(name!)", forState: UIControlState.Normal)
    }
}

